var wachtId;

function initializeMap(latitude, longitude) {
    var latLng = new google.maps.LatLng(latitude, longitude);       

    var mapProp = {
        center: latLng,
        zoom: 25,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };


    var map=new google.maps.Map(document.getElementById("map"),mapProp);

    var marker = new google.maps.Marker({
        position: latLng,
        animamtion: google.maps.Animation.BOUNCE
    });

    marker.setMap(map) 

    return map;
}


function onSuccess(position) {
    var element = document.getElementById('geolocation');
    element.innerHTML = 
        '<br>Latitude: '          + position.coords.latitude +
        '<br>Logintude: '         + position.coords.longitude +
        '<br>Altitude: '          + position.coords.altitude +
        '<br>Altitude Accuracy: ' + position.coords.altitudeAccuracy +
        '<br>Speed: '             + position.coords.speed +
        '<br>Timestamp: '         + position.timestamp;

    initializeMap(position.coords.latitude, position.coords.longitude);        
}

function onError(error) {
    var message =  'code: ' + error.code + ' message: ' + error.message
    alert(message)
}

function stopWatchPosition() {
    navigator.geolocation.clearWatch(watchId);
}

function initialize() {
    var options = {enableHighAccuracy: true, timeout: 30000, maximumAge: 30000};    

    navigator.geolocation.getCurrentPosition(onSuccess, onError, options);

    wachtId = navigator.geolocation.watchPosition(onSuccess, onError, options);
}

initialize();
